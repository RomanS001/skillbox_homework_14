//
//  PersistenceTask4.swift
//  Skillbox_HomeWork_14
//
//  Created by Roman on 13.09.2020.
//  Copyright © 2020 Roman Sundurov. All rights reserved.
//

import Foundation
import RealmSwift

class ForecastWeatherFor7Days: Object{
    @objc dynamic var days1: String = ""
    @objc dynamic var days2: String = ""
    @objc dynamic var days3: String = ""
    @objc dynamic var days4: String = ""
    @objc dynamic var days5: String = ""
    @objc dynamic var days6: String = ""
    @objc dynamic var days7: String = ""
}

private let realm = try! Realm()

func addForecastToRealmData(daysArray: [Int : String]) {
    let addforecastArray = ForecastWeatherFor7Days()
    addforecastArray.days1 = daysArray[1]!
    addforecastArray.days2 = daysArray[2]!
    addforecastArray.days3 = daysArray[3]!
    addforecastArray.days4 = daysArray[4]!
    addforecastArray.days5 = daysArray[5]!
    addforecastArray.days6 = daysArray[6]!
    addforecastArray.days7 = daysArray[7]!
    print("ToDownloadData: \(addforecastArray)")
    try! realm.write {
        realm.add(addforecastArray)
    }
}

func getForecastRealmData() -> [Int: String] {
    let forecastWeatherFor7DaysObject = realm.objects(ForecastWeatherFor7Days.self)
    if forecastWeatherFor7DaysObject.isEmpty { return [0: "Error"] } else {
        var allforecastArray: [Int: String] = [:]
        allforecastArray[1] = (forecastWeatherFor7DaysObject.first!.days1)
        allforecastArray[2] = (forecastWeatherFor7DaysObject.first!.days2)
        allforecastArray[3] = (forecastWeatherFor7DaysObject.first!.days3)
        allforecastArray[4] = (forecastWeatherFor7DaysObject.first!.days4)
        allforecastArray[5] = (forecastWeatherFor7DaysObject.first!.days5)
        allforecastArray[6] = (forecastWeatherFor7DaysObject.first!.days6)
        allforecastArray[7] = (forecastWeatherFor7DaysObject.first!.days7)
        print("DownloadedData: \(allforecastArray)")
        return allforecastArray
    }
}
