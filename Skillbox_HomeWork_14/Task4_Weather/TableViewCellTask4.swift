//
//  TableViewCellTask4.swift
//  Skillbox_HomeWork_14
//
//  Created by Roman on 13.09.2020.
//  Copyright © 2020 Roman Sundurov. All rights reserved.
//

import UIKit

class TableViewCellTask4: UITableViewCell {

    @IBOutlet weak var indexOfDay: UILabel!
    @IBOutlet weak var weatherOfDay: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
