//
//  WeatherClass.swift
//  Skillbox_HomeWork_14
//
//  Created by Roman on 13.09.2020.
//  Copyright © 2020 Roman Sundurov. All rights reserved.
//

import Foundation

class Weather7DaysClass {
    
    var weather7DaysDictionary = [Int: String]()
    var index: Int = 0
    
    init?(arrayData: NSDictionary) {
//        print("2: \(arrayData)")
        
        for (key, data) in arrayData {
            if key as! String == "daily" {
                for data2 in data as! NSArray {
                    for (key, data3) in data2 as! NSDictionary where key as! String == "weather" {
                        let data3New = (data3 as! NSArray)[0] as! NSDictionary
//                        print("4: \(data3New["description"])")
                        let data3NewString: String = data3New["description"] as! String
                        weather7DaysDictionary[index] = data3NewString
                        index += 1
                    }
                }
            }
        }
    }
}
