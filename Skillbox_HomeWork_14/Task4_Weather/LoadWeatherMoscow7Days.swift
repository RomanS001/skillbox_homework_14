//
//  LoadWeatherMoscow7Days.swift
//  Skillbox_HomeWork_14
//
//  Created by Roman on 13.09.2020.
//  Copyright © 2020 Roman Sundurov. All rights reserved.
//

import Foundation
import Alamofire

func funcLoadWeatherMoscow7Days(completion: @escaping (NSDictionary) -> Void) {
    AF.request("https://api.openweathermap.org/data/2.5/onecall?lat=55.7522200&lon=37.6155600&exclude=current,minutely,hourly&appid=a0116d2e3f9a21006e26b074f6afa787").responseJSON {
        response in
        if let object = response.value,
            let jsonDict = object as? NSDictionary {
//            print("1: \(jsonDict["daily"])")
    
            let weatherDict = (Weather7DaysClass(arrayData: jsonDict)?.weather7DaysDictionary)! as NSDictionary
//            print("11: \(weatherDict)")

            completion(weatherDict)
        }
    }
}
