//
//  ViewControllerTask4.swift
//  Skillbox_HomeWork_14
//
//  Created by Roman on 13.09.2020.
//  Copyright © 2020 Roman Sundurov. All rights reserved.
//

import UIKit

class ViewControllerTask4: UIViewController {
    
        @IBOutlet weak var tableView: UITableView!
        
        var weatherDict = [Int: String]()
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            let weatherPersistence = getForecastRealmData()
            if weatherPersistence[0] == "Error" {
                print("weatherPersistence[0] = error, not data")
            } else {
                weatherDict = weatherPersistence
                tableView.reloadData()
            }
            
            funcLoadWeatherMoscow7Days(completion: { weatherData in
                self.weatherDict = weatherData as! [Int : String]
                addForecastToRealmData(daysArray: self.weatherDict)
                self.tableView.reloadData()
            })
        }
    }
        
        extension ViewControllerTask4: UITableViewDelegate, UITableViewDataSource{
            func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                return weatherDict.count
            }
            
            func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CellForWeatherForecast") as! TableViewCellTask4
                if weatherDict.isEmpty { return cell } else {
                    let index = indexPath.row + 1
                    cell.indexOfDay.text = String(index)
                    cell.weatherOfDay.text = weatherDict[index]
                }
                return cell
            }
}
