//
//  PersistenceTask3.swift
//  Skillbox_HomeWork_14
//
//  Created by Roman on 12.09.2020.
//  Copyright © 2020 Roman Sundurov. All rights reserved.
//

import Foundation
import CoreData

class ToDoListCoreDataClass {
}

func addCaseNameInCoreData(text: String, appDelegateInPersistence: AppDelegate) {
    let context = appDelegateInPersistence.persistentContainer.viewContext
    let entity = NSEntityDescription.entity(forEntityName: "ToDoListCoreData", in: context)
    let newToDoListCoreData = NSManagedObject(entity: entity!, insertInto: context)
    newToDoListCoreData.setValue(text, forKey: "caseListCoreData")
    newToDoListCoreData.setValue(false, forKey: "checkBoxStatus")
    do {
        try context.save()
    } catch {
        print("addCaseNameInCoreData failed")
    }
}

func getCoreData(appDelegateInPersistence: AppDelegate) -> ([Int: String], [Int: Bool])? {
    var arrayText: [Int: String] = [:]
    var arrayCheckBoxStatus: [Int: Bool] = [:]
    let context = appDelegateInPersistence.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ToDoListCoreData")
    request.returnsObjectsAsFaults = false
    do {
        let result = try context.fetch(request)
        var index: Int = -1
        for data in result as! [NSManagedObject]{
            index += 1
            arrayText[index] = data.value(forKey: "caseListCoreData") as! String
            arrayCheckBoxStatus[index] = data.value(forKey: "checkBoxStatus") as! Bool
        }
        let dataOut = (arrayText, arrayCheckBoxStatus)
        print("dataOut in getCoreData = \(dataOut)")
        return dataOut
    } catch {
        print("getCoreData failed")
        return nil
    }
}

func deleteCoreData(tag: Int, appDelegateInPersistence: AppDelegate) {
    let context = appDelegateInPersistence.persistentContainer.viewContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ToDoListCoreData")
    request.returnsObjectsAsFaults = false
    do {
        let result = try context.fetch(request)
        var caseID: Int = -1
        for data in result as! [NSManagedObject] {
            caseID += 1
            if caseID == tag {
                context.delete(data)
                try context.save()
            }
        }
    } catch {
        print("deleteRealmDataTask3 failed")
    }
}

func updateCoreDataText(tag: Int, text: String, appDelegateInPersistence: AppDelegate) {
    let context = appDelegateInPersistence.persistentContainer.viewContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ToDoListCoreData")
    request.returnsObjectsAsFaults = false
    do {
        let result = try context.fetch(request)
        var caseID: Int = -1
        for data in result as! [NSManagedObject] {
            caseID += 1
            if caseID == tag {
                data.setValue(text, forKey: "caseListCoreData")
                try context.save()
            }
        }
    } catch {
        print("updateCoreDataText failed")
    }
}

func updateCoreDataCheckBoxStatus(tag: Int, type: Bool, appDelegateInPersistence: AppDelegate) {
    let context = appDelegateInPersistence.persistentContainer.viewContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ToDoListCoreData")
    request.returnsObjectsAsFaults = false
    do {
        let result = try context.fetch(request)
        var caseID: Int = -1
        for data in result as! [NSManagedObject] {
            caseID += 1
            if caseID == tag {
                data.setValue(type, forKey: "checkBoxStatus")
                try context.save()
            }
        }
    } catch {
        print("updateCoreDataCheckBoxStatus failed")
    }
}

