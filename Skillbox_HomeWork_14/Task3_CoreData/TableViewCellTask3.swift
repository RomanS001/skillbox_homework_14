//
//  TableViewCellTask3.swift
//  Skillbox_HomeWork_14
//
//  Created by Roman on 09.09.2020.
//  Copyright © 2020 Roman Sundurov. All rights reserved.
//

import UIKit
import SimpleCheckbox

protocol SpecialCellDelegateTask3: class {
    func buttonDeleteItemHandlerTask3(_ tag: Int)
    func optButtonHandlerTask3(_ tag: Int)
    func checkBoxStatus(_ tag: Int,_ type: Bool)
}

class TableViewCellTask3: UITableViewCell {
    
    @IBOutlet var nameTaskTask3: UILabel!
    @IBOutlet var buttonDeleteItemObjectTask3: UIButton!
    @IBOutlet var checkBoxObject: Checkbox!
    
    var cellDelegateTask3: SpecialCellDelegateTask3?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        checkBoxObject.addTarget(self, action: #selector(checkboxValueChangedTask3(sender:)), for: .valueChanged)
    }
    
    @objc func checkboxValueChangedTask3(sender: Checkbox) {
        switch checkBoxObject.isChecked {
        case true:
            cellDelegateTask3?.checkBoxStatus(checkBoxObject.tag, true)
        case false:
            cellDelegateTask3?.checkBoxStatus(checkBoxObject.tag, false)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }


    @IBAction func buttonDeleteItemTask3(_ sender: UIButton) {
        print("tag buttonDeleteItemTask3= \(sender.tag)")
        cellDelegateTask3?.buttonDeleteItemHandlerTask3(sender.tag)
    }
    
    @IBAction func optButton(_ sender: Any) {
        print("tag optButton= \(checkBoxObject.tag)")
        cellDelegateTask3?.optButtonHandlerTask3(checkBoxObject.tag)
    }
    
}
