//
//  ViewControllerTask3.swift
//  Skillbox_HomeWork_14
//
//  Created by Roman on 09.09.2020.
//  Copyright © 2020 Roman Sundurov. All rights reserved.
//

import UIKit

class ViewControllerTask3: UIViewController {

    @IBOutlet var tableViewTask3: UITableView!
    @IBOutlet var textFieldNewItemTask3: UITextField!
    
    var newTableTextArrayTask3: [Int: String] = [:]
    var newTableCheckBoxStatusArrayTask3: [Int: Bool] = [:]
    
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    

    override func viewDidLoad() {
        super.viewDidLoad()
        tableUpdate()
    }
    
    func tableUpdate() {
        (newTableTextArrayTask3, newTableCheckBoxStatusArrayTask3) = getCoreData(appDelegateInPersistence: appDelegate)!
        tableViewTask3.reloadData()
    }


    @IBAction func buttonAddNewItemTask3(_ sender: Any) {
        if textFieldNewItemTask3.text != "" {
            addCaseNameInCoreData(text: textFieldNewItemTask3.text!, appDelegateInPersistence: appDelegate)
        }
        tableUpdate()
        textFieldNewItemTask3.text = ""
    }
}
    

extension ViewControllerTask3: UITableViewDelegate, UITableViewDataSource, SpecialCellDelegateTask3 {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newTableTextArrayTask3.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableCellTask3") as! TableViewCellTask3
        cell.nameTaskTask3.text = newTableTextArrayTask3[indexPath.row]
        cell.cellDelegateTask3 = self
        cell.buttonDeleteItemObjectTask3.tag = indexPath.row
        cell.checkBoxObject.isChecked = newTableCheckBoxStatusArrayTask3[indexPath.row]!
        cell.checkBoxObject.tag = indexPath.row
//        print("data indexPath.row: \(indexPath.row)")
//        print("data newTableDataArrayTask3: \(newTableTextArrayTask3)")
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func buttonDeleteItemHandlerTask3(_ tag: Int) {
        deleteCoreData(tag: tag, appDelegateInPersistence: appDelegate)
        tableUpdate()
    }
    
    func optButtonHandlerTask3(_ tag: Int) {
        var newNameTask3: UITextField?
        let alertController = UIAlertController(
           title: "Переименовать задачу",
           message: "",
           preferredStyle: .alert)
        let loginAction = UIAlertAction(
            title: "Переименовать", style: .default) {
           (action) -> Void in
           if newNameTask3!.text != "" {
            updateCoreDataText(tag: tag, text: newNameTask3!.text!, appDelegateInPersistence: self.appDelegate)
//            print("newNameTask.text = \(newNameTask3!.text)")
            } else {
            print("newNameTask.text is empty")
            }
            self.tableUpdate()
        }
        alertController.addTextField {
           (txtUsername) -> Void in
           newNameTask3 = txtUsername
           newNameTask3!.placeholder = "Введите новое название"
        }
        alertController.addAction(loginAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func checkBoxStatus(_ tag: Int,_ type: Bool) {
        updateCoreDataCheckBoxStatus(tag: tag, type: type, appDelegateInPersistence: appDelegate)
        tableUpdate()
//        print("checkbox is: \(type), tag= \(tag)")
    }
    
}
