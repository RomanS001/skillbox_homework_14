//
//  PersistenceTask2.swift
//  Skillbox_HomeWork_14
//
//  Created by Roman on 09.09.2020.
//  Copyright © 2020 Roman Sundurov. All rights reserved.
//

import Foundation
import RealmSwift

class ToDoList: Object{
    @objc dynamic var caseName = ""
    @objc dynamic var checkBoxStatus: Bool = false
}

private let realm = try! Realm()

func addCaseName(name: String) {
    let toDo = ToDoList()
    toDo.caseName = name
    try! realm.write {
        realm.add(toDo)
    }
}

func getRealmData() -> Results<ToDoList> {
    let allToDo = realm.objects(ToDoList.self)
    return allToDo
}

func deleteRealmData(tag: Int) {
    let allToDo = realm.objects(ToDoList.self)
//    print(allToDo[tag].caseName)
    try! realm.write {
        realm.delete(allToDo[tag])
    }
}

func updateRealmDataText(tag: Int, text: String) {
    let allToDo = realm.objects(ToDoList.self)
    try! realm.write {
        allToDo[tag].caseName = text
    }
}

func checkBoxStatusRealm(_ tag: Int,_ type: Bool) {
    let allToDo = realm.objects(ToDoList.self)
    try! realm.write {
        allToDo[tag].checkBoxStatus = type
    }
}
