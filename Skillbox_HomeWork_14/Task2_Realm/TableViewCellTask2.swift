//
//  TableViewCellTask2.swift
//  Skillbox_HomeWork_14
//
//  Created by Roman on 09.09.2020.
//  Copyright © 2020 Roman Sundurov. All rights reserved.
//

import UIKit
import SimpleCheckbox

protocol SpecialCellDelegate: class {
    func buttonDeleteItemHandler(_ tag: Int)
    func optButtonHandler(_ tag: Int)
    func checkBoxStatus(_ tag: Int,_ type: Bool)
}

class TableViewCellTask2: UITableViewCell {
    @IBOutlet var nameTask: UILabel!
    @IBOutlet var buttonDeleteItemObject: UIButton!
    @IBOutlet var checkBoxObject: Checkbox!
    
    var cellDelegate: SpecialCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        checkBoxObject.addTarget(self, action: #selector(checkboxValueChanged(sender:)), for: .valueChanged)
    }
    
    @objc func checkboxValueChanged(sender: Checkbox) {
        switch checkBoxObject.isChecked {
        case true:
            cellDelegate?.checkBoxStatus(checkBoxObject.tag, true)
        case false:
            cellDelegate?.checkBoxStatus(checkBoxObject.tag, false)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func buttonDeleteItem(_ sender: UIButton) {
        cellDelegate?.buttonDeleteItemHandler(buttonDeleteItemObject.tag)
    }
    
    @IBAction func optButton(_ sender: UIButton) {
        cellDelegate?.optButtonHandler(buttonDeleteItemObject.tag)
    }
    
}
