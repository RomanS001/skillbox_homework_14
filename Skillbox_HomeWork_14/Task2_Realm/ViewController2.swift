//
//  ViewController2.swift
//  Skillbox_HomeWork_14
//
//  Created by Roman on 08.09.2020.
//  Copyright © 2020 Roman Sundurov. All rights reserved.
//

import UIKit

class ViewController2: UIViewController {
    @IBOutlet var tableView: UITableView!
    @IBOutlet var textFieldNewItem: UITextField!
    
    var newTableDataArrayCaseName: [String] = [""]
    var newTableDataArrayType: [Bool] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableUpdate()
    }
    
    @IBAction func buttonAddNewItem(_ sender: Any) {
        if textFieldNewItem.text != "" {
            addCaseName(name: textFieldNewItem.text!)
        }
        tableUpdate()
        textFieldNewItem.text = ""
    }
    
    func tableUpdate() {
        let newTableData = getRealmData()
        newTableDataArrayCaseName = []
        newTableDataArrayType = []
        for n in newTableData{
            newTableDataArrayCaseName.append(n.caseName)
            newTableDataArrayType.append(n.checkBoxStatus)
//            print(n.caseName)
        }
        tableView.reloadData()
    }
}

extension ViewController2: UITableViewDelegate, UITableViewDataSource, SpecialCellDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newTableDataArrayCaseName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableCellTask2") as! TableViewCellTask2
        cell.nameTask.text = newTableDataArrayCaseName[indexPath.row]
        cell.checkBoxObject.isChecked = newTableDataArrayType[indexPath.row]
        cell.cellDelegate = self
        cell.buttonDeleteItemObject.tag = indexPath.row
        cell.checkBoxObject.tag = indexPath.row
        cell.checkBoxObject.checkmarkStyle = .tick
        return cell
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func buttonDeleteItemHandler(_ tag: Int) {
        deleteRealmData(tag: tag)
        tableUpdate()
    }
    
    func optButtonHandler(_ tag: Int) {
        var newNameTask: UITextField?
        let alertController = UIAlertController(
           title: "Переименовать задачу",
           message: "",
           preferredStyle: .alert)
        let loginAction = UIAlertAction(
        title: "Переименовать", style: .default) {
           (action) -> Void in
           if newNameTask!.text != "" {
            updateRealmDataText(tag: tag, text: newNameTask!.text!)
            print("newNameTask.text = \(newNameTask!.text)")
            } else {
            print("newNameTask.text is empty")
            }
            self.tableUpdate()
        }
        alertController.addTextField {
           (txtUsername) -> Void in
           newNameTask = txtUsername
           newNameTask!.placeholder = "Введите новое название"
        }
        alertController.addAction(loginAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func checkBoxStatus(_ tag: Int,_ type: Bool) {
        checkBoxStatusRealm(tag, type)
        tableUpdate()
//        print("checkbox is: \(type), tag= \(tag)")
    }

}
