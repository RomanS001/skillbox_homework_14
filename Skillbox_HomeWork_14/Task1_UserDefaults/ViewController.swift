//
//  ViewController.swift
//  Skillbox_HomeWork_14
//
//  Created by Roman on 02.09.2020.
//  Copyright © 2020 Roman Sundurov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var textFieldName: UITextField!
    @IBOutlet var textFieldSurname: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldName.text = Persistence.shared.userName
        textFieldSurname.text = Persistence.shared.userSurname
        // Do any additional setup after loading the view.
    }

    
    @IBAction func ButtonSaveDate(_ sender: Any) {
        if textFieldName.text == "" || textFieldSurname.text == "" {
//            print("Введите все данные")
        }
        else {
            Persistence.shared.userName = textFieldName.text
            Persistence.shared.userSurname = textFieldSurname.text
//            print("Данные сохранены")
        }
    }
    
}

