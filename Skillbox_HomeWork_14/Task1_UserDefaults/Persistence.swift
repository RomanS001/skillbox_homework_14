//
//  Persistence.swift
//  Skillbox_HomeWork_14
//
//  Created by Roman on 07.09.2020.
//  Copyright © 2020 Roman Sundurov. All rights reserved.
//

import Foundation

class Persistence {
    static let shared = Persistence()
    
    private let kUserNameKey = "Persistence1.kUserNameKey"
    private let kUserSurnameKey = "Persistence1.kUserSurnameKey"
    
    var userName: String?{
        set { UserDefaults.standard.set(newValue, forKey: kUserNameKey) }
        get { return UserDefaults.standard.string(forKey: kUserNameKey) }
    }
    var userSurname: String?{
        set { UserDefaults.standard.set(newValue, forKey: kUserSurnameKey) }
        get { return UserDefaults.standard.string(forKey: kUserSurnameKey) }
    }
    
}
